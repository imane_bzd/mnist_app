FROM python:3

RUN mkdir home_template

RUN cd home_template

RUN mkdir uploads

RUN mkdir templates

COPY requirements.txt .

COPY index.html .\templates\.

RUN apt-get update

RUN pip3 install -r requirements.txt

COPY main.py .

COPY mnist.h5 .

COPY index.py .

COPY app.py .

EXPOSE 5000 

ENTRYPOINT export FLASK_APP=index.py && flask run


