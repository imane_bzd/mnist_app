# Packaging et déploiement local d'un pipeline de classification MNIST

Ce programme consiste à développer une interface web simple permettant 
de télécharger une image Mnist http://yann.lecun.com/exdb/mnist et de fournir le résulat de sa classification 

## Installation

Ces instructions permettent d'obtenir une version locale de l'interface web. 
Il existe deux installations possibles : une installation en récupérant l'image docker à partir du Docker Hub et une seconde installation sans docker en récupérant le dépot gitlab.

### Lancement du programme avec docker 

Si vous choisissez cette installation, **Docker est requis**. 
Docker a été utilisé afin de limiter les différences de configuration entre les machines. 
Une fois Docker installé, il suffit de suivre les étapes suivantes : 
1. Créez votre compte sur https://hub.docker.com/ . Il faut garder son username pour l'étape 3
2. Ouvrez un terminal dans votre PC
3. Tapez la commande `docker login -u YOUR-USER-NAME` dans votre terminal
4. Récupérer l'image que déja créée à partir de mon repo  ` docker pull imane213/my-first-repo/mnist_app_2` . _Cette étape peut être un peu lente._
5. Assurez-vous que l'image existe bien chez vous en local en tapant la commande `docker images`. Le nom de l'image doit apparaitre si l'étape 4 s'est bien déroulée 
6. Lancez l'image avec la commande: `docker run mnist_app_2`
7. Un lien de la forme http://127.0.0.1:5000/ est généré. Copiez ce lien dans votre navigateur. 
Dans la page qui s'affiche vous pouvez télécharger une image mnist via le bouton **upload** et le résultat de sa classification s'obtient en cliquant sur le bouton **submit**

### Lancement du programme sans docker 

Si vous choisissez cette option, il suffit de :
1. Récupérer ce dépot via la commande `git clone https://gitlab.com/imane_bzd/mnist_app.git`
2. Installer les packages python requis en tapant `pip3 install -r requirements.txt`
3. Taper la commande lancement `chmod +x run_script.sh && ./run_script.sh`
4. Un lien de la forme http://127.0.0.1:5000/ est généré. Copiez ce lien dans votre navigateur. 
Dans la page qui s'affiche vous pouvez télécharger une image mnist via le bouton **upload** et le résultat de sa classification s'obtient en cliquant sur le bouton **submit**


## Modèle de classification
Le modèle de classification utilisé est chargé à partir de `mnist.h5`. 
Si vous préférez utiliser un autre modèle, il suffit de remplacer `mnist.h5` par `YOUR-MODEL-NAME.h5`. 
En utilisant un autre modèle, il faut vérifier que le **pre-processing** de l'image dans la fonction `getPrediction()` du `main.py` est compatible.

## Gestion des entrées et sorties de données
### Données entrantes

Pour l'entrée des données, chaque image téléchargée dans l'interface est stockée dans le répertoire `/uploads`.
Dans ce dépôt gitlab, `/uploads` contient 6 images _png_ Mnist qui serviront dans les tests.
Pour nettoyer le répertoire `/uploads` il suffit de lancer la commande `chmod +x run_script.sh && ./clean_script.sh`. 
**ATTENTION** cette commande supprime tout le contenu de `/uploads`.

### Données sortantes

C'est la fonction `getPrediction()` du `main.py` qui fournit la prédiction pour chaque image. Cette fonction prend en entrée le nom d'une image qui doit absolument exister dans le répertoire `/uploads` et fournit en sortie _(valeur de retour)_ la prédiction sous forme d'une chaine de caratères.  

## Interface

Le fichier `app.py` contient la configuration basique de l'application. 
Le script `index.py` fait l'intermediaire entre l'interface et le modèle de classification. Ce script télécharge l'image, la stocke dans le répertoire `/uploads`, récupère la prédiction et affiche cette prédiction.
Le fichier `index.html` configure l'affichage de la page web.

## Les tests

Le fichier `run_test.py` permet de tester la prédiction sur 5 images contenues dans le répertoire `uploads.py`.
Pour lancer les tests il suffit de taper `python3 run_test.py`  



















