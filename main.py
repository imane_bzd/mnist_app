import os 
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.models import load_model

#Loading model
model_name = 'mnist.h5'
mnist_model = load_model(model_name)

#Returns prediction for a given image
def getPrediction(filename):
    # loads image from 'uploads' repo
    img = load_img('./uploads/'+filename, grayscale=True, target_size=(28, 28))
    # converts to array
    img = img_to_array(img)
    # reshapes into a single sample with 1 channel
    img = img.reshape(1, 28, 28, 1)
    # flattens image
    img = img.flatten('C')
    # prepares pixel data
    img = img.astype('float32')
    #rescales the image from 0-255 to 0-1
    img = img / 255.0
    # reshapes 
    img = img.reshape(1, 784)
    # gets prediction
    digit = mnist_model.predict_classes(img)
    return str(digit[0]) 

